import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:tenizo/route.dart';
import 'styles/app_style.dart';


void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      MaterialApp(
        title: 'Tennizo',
        theme: ThemeData(
            appBarTheme: AppBarTheme(
                color: AppStyle.color_AppBar,
                textTheme: TextTheme(
                  title: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Rajdhani'),
                )),
            fontFamily: 'Rajdhani',
            // Define the default TextTheme. Use this to specify the default
            textTheme: TextTheme(
              headline: TextStyle(fontSize: 35.0, fontWeight: FontWeight.w600),
              title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
              body1: TextStyle(
                fontSize: 14.0,
              ),
            ),
            iconTheme: IconThemeData(size: 40)),
        onGenerateRoute: (settings) => setRoute(settings),
        debugShowCheckedModeBanner: false,
      ),
    );
  });
}
