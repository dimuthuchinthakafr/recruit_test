import 'package:tenizo/tennizo_base_controller.dart';
import 'package:flutter/material.dart';

class TermsConditionsPage extends StatefulWidget {
  final arguments;
  final Function onSelected;
  @override
  _TermsConditionsPageState createState() => _TermsConditionsPageState();
  TermsConditionsPage({Key key, this.onSelected, this.arguments})
      : super(key: key);
}

class _TermsConditionsPageState extends State<TermsConditionsPage>
    with BaseControllerListner {
  BaseController controller;

  @override
  void initState() {
    super.initState();
    controller = new BaseController(this);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MediaQuery(
              data: MediaQueryData(),
              child: Text('Terms and Conditions',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700))),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: MediaQuery(
          data: MediaQueryData(),
          child: SingleChildScrollView(
            //main body container
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  children: <Widget>[
                    //title
                    Container(
                      width: MediaQuery.of(context).size.width,
                      // decoration: BoxDecoration(color: Colors.red),
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "TenniZo",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),

                    //subtitle
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(
                          'Terms & Conditions',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),

                    //paragraph
                    Container(
                      width: MediaQuery.of(context).size.width,
                      //decoration: BoxDecoration(color: Colors.red),
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          '     These Terms and Conditions shall manage your use of “TenniZo” application service (hereinafter referred to as the "Service") provided by Arite Co., Ltd. (hereinafter referred to as the “Company”). Please use this service after agreeing to these Terms.',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.normal),
                              textAlign: TextAlign.justify,
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "Article 1(Definition)",
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "The definitions of Terms used in these are as follows,",
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Table(
                          border: TableBorder.all(),
                          columnWidths: {
                            0: FlexColumnWidth(2.5),
                            1: FlexColumnWidth(4),
                          },
                          children: [
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(1) Service'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Services managed by the Company and related services.',
                                    style: TextStyle(height: 1.5),
                                  ),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(2) Contents'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                      'Texts, audios, still images, videos, software programs, codes, etc. provided on this service. (including posting information)',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(3) User'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Those who uses this service',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(4) Registered User'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Those who have completed User registration for this service',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(5) Policyholder'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Those who have contracted with our Company for a usage contract under these Terms and conditions.',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(6) ID'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Unique character strings to registered Users for using this service',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(7) Password'),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('A cipher uniquely set by the registered User in correspondence with the ID.',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(8) Private information',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                      'Information that can identify the specific individual by address, name, occupation, and telephone number etc.',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ]),
                            TableRow(children: [
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('(9) Registered information',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                              TableCell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('Information registered in this service by registered Users (excluding posting information)',
                                      style: TextStyle(height: 1.5)),
                                ),
                              ),
                            ])
                          ],
                        ),
                      ),
                    ),

                    //  topic 2 ---------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          "Article 2（Agreeing to these Terms）",
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text('1. The Users shall be able to use this Service after agreeing to these Terms of use.',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.normal,
                                height: 1.5)),
                                
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '2. When the User downloads this Service to a smartphone or other devices and agrees to these Terms, a User agreement shall be established between the User and the Company under the provisions of this Term.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '3. For minor User, please use this service after obtaining the consent of a parent or other legal representative.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '4. If a minor User uses this service without the consent of legal representative or fakes the age or uses fraud to convince others that they are capable of using, he/she shall not be able to cancel any legal action related to this Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '5. If a minor User who agreed to Terms and use this service after reaching adulthood, the User is deemed to have confirmed all legal actions related to this Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 3----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 3 (Amendment of Terms)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '1. The Company shall be able to amend the provisions of this agreement without obtaining the consent of the Users.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '2. When the Company revises these Terms, the Company will notify the User about the contents under the Company’s predetermined method.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '3. The amendment of the Terms shall be effective from the time when the Company makes a notification according to the preceding paragraph.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '4. The Company assumes Users as having agreed to the altered Terms of use at the time when they use this Service after the amendment of Terms.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 4----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 4 (Account Management)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '1. The User shall register and manage the information registered (Hereafter referred to as “Registered Information”, including e-mail address, ID/password, etc.)at the time of use at his / her own risk. The User shall not let this be used by a third party, or lend, transfer, or change the name.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '2. If the Service is used based on the registered information, the Company shall be assumed that it was used by the person who registered for use. The results from such use and all responsibilities associated with it belong to the person who registered for use.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '3. In the event of damage to the Company or a third party due to the unauthorized use of registered information, the User shall compensate the Company and the third party for such damage.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '4. The management of registered information shall be under the responsibility of the User, and the Company shall not be liable for any disadvantages and damages incurred by the User due to the registered information was inaccurate or false.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '5. In cases where the registered information is found to be stolen or used by a third party, the User shall immediately notify the Company to that effect and follow the instructions provided by the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 5----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 5 (Handling of personal information, etc.)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          ' Personal information and User information shall be handled properly in accordance with the “TenniZo Privacy Policy” separately established by the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 6----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 6 (Prohibited Acts)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'When using this Service, the Company prohibits the following actions for Users. If the Company found that the User has violated the prohibited matters, he/she shall be suspended the use and take other measures deemed necessary by the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(1) Defaming the character or trustworthiness of the Company or a third party; slandering or threatening the Company or a third party or engaging in any similar act.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(2) Any actions that infringe or imply infringement on the estate of the Company or a third party.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(3) Acts that are detrimental or cause loss to the Company and a third party.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(4) Threatening acts against the Company or a third party.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(5) Acts that use or provide any harmful software program or computer virus.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(6) Acts that put excessive burdens on the infrastructure equipment for this Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(7) Acts that analyzing program, duplicating, and editing data of this Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(8) Act that attempting to access our Services by means other than the interface provided by our Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(9) An act of one User acquiring multiple User IDs',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '(10) In addition to the above, other acts that may be deemed inappropriate by the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 7----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 7（Disclaimer）',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '1. The Company shall not be held liable in any way for damages suffered by Users due to a change, suspension, or termination of the Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '2. The Company shall not be involved in any usage environment of the User and does not take any responsibility.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '3. The Company shall not guarantee that this Service fits the specific purpose of the User or having the expected function, product value, accuracy, usefulness or the use of this Service by the User conforms to the laws applicable to the User or the internal rules of the industry associations, etc. And there is no guarantee that no problems will occur.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '4. The Company shall not guarantee that this Service is compatible with all devices. The User shall acknowledge in advance that there may be problems with the operation of this Service due to the OS version upgrade of the device that is used for this Service. And the Company shall not guarantee that such defects will be resolved by correcting the program by the Company when such defects occur.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '5. The User shall acknowledge in advance that the use of this Service may be restricted in part due to changes in the Terms of Service and operating policies of Service stores such as AppStore and Google Play.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '6. The Company shall not be liable for any damages incurred directly or indirectly to the User by using this Service.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '7. The Company shall not be liable even if the Company has been informed in advance of the possibility of damages related to the missing business opportunities or interruption of business affairs or any other damage (including indirect damages and lost profits) that occurred to Users or any third parties.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '8. The provisions of paragraph 1 to the previous paragraph do not apply when there is intentional or gross negligence in the Company or when the contractor falls under the Consumer Contract Law.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '9. Even if the preceding paragraph applies, the Company shall not be liable for any damages caused by special circumstances among the damages caused to the User by negligence (excluding gross negligence).',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '10. If the Company is liable for damages in relation to the use of this Service, it shall be liable for damages up to the amount of usage received from the User in the month in which such damage occurs.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '11. The Company shall not liable for any disputes or troubles between Users. Even if a problem occurs between the Users, both parties shall be responsible for the problem and no claim shall be made to the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '12. In the event of damage to other Users or a dispute with a third party in relation to the use of this Service, the User shall compensate for such damage at his / her own expense and shall not cause any inconvenience or damage to the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "13. If the Company received a claim for damages from a third party due to the User's actions, it shall be resolved at the User's expense (legal fee) and responsibility. The User shall pay the Company all costs including the damages (including legal fees and lost profits) if the Company pays damages to the third party.",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '14. If a User causes loss or damage to the Company in connection with the use of this Service, the User shall be liable to pay compensation for such loss or damages (including lawsuit and legal fees).',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 8----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 8 (Advertisement)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'The User acknowledges and accepts that any advertisements may be included on the Service and that the Company or any of our affiliates may place any advertisements. The form and scope of advertising on this Service will be changed by the Company from time to time.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 9----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 9 (Prohibition of Transfer of Rights)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '1. The Users shall not transfer all or part of the status and the rights or obligations based on these Terms to a third party without the prior written consent of the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          "2. The Company may transfer all or part of the Service to a third party at the Company's discretion including the User's account within the scope of the Transfer of Rights. All of the rights shall be transferred to the transferee.",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 10----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 10 (Separability) ',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'In the event that any provision of these Terms is held to be invalid or unenforceable, then thatprovision will be limited or eliminated to the minimum extent necessary, and the remaining provisions of these Terms will remain in full force and effect.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 11----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 11 (How to contact us)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10.0,
                      ),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'For inquiries to the Company regarding the Service shall be sent from the screen set in the Service or by a method specified separately by the Company.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    //topic 12----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Text(
                          'Article 12 (Governing Law, Jurisdiction)',
                          style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '1. The validity, interpretation and application of this these Terms shall be governed by Japanese laws.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 15.0, left: 10, right: 10),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          '2. In case of any dispute that may arise between the Company and the User, the Sapporo District Court shall be the agreed court of first instance with exclusive jurisdiction.',
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                              height: 1.5),
                        ),
                      ),
                    ),

                    // Final----------------------
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 50),
                            child: Text(
                              'Effective from September 1, 2019',
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                  height: 1.5),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  @override
  resultFunction(func, subFunc, response) {}
}
