import 'dart:io';
import 'dart:ui';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:tenizo/custom/commom_popup.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';
import 'package:google_maps_webservice/places.dart' as places;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';
import 'package:camera/camera.dart';

import '../const.dart';

class StadiumEdit extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _StadiumEditState createState() => _StadiumEditState();
  StadiumEdit({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _StadiumEditState extends State<StadiumEdit> with BaseControllerListner {
  BaseController controller;

  TextEditingController _nameFieldController = TextEditingController();
  TextEditingController _adressFieldController = TextEditingController();
  TextEditingController _reservationFieldController = TextEditingController();
  TextEditingController _phonenumFieldController = TextEditingController();
  TextEditingController _nocourtFieldController = TextEditingController();

  List<DropdownMenuItem<String>> _dropDownMenuItems;

  List _countryCode = ["", "+94", "+81", "+1", "+44", "+52", "+91"];

  Future<File> imageFile;
  File pickedImage;
  var deleteImage;

  String sqlData;
  String _currentcuntrycode;
  String name = "";
  String _imageFilePath;
  bool _imageAvailability;

  String _city;
  double _lat = 0;
  double _long = 0;
  bool _mapDisplay = false;
  GoogleMapController mapController;
  LatLng _mapLocation = LatLng(0, 0);
  places.GoogleMapsPlaces _places =
      places.GoogleMapsPlaces(apiKey: Const.mapsAPIKey);
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  var param = [
    "UPDATE stadium SET stadium_name = ?,stadium_address = ?,contact_number = ?,no_of_courts = ?,reservation_url = ?, stadium_image= ?, Stadium_Lat = ?, Stadium_Lon =?  where stadium_id = ?",
    newData
  ];

  var phonNumber = '';
  static var newData = ['', '', '', 0, '', '', '', '', ''];
  static var stdName = "";
  static var stadiumId = "";
  static var stadiumAddress = "";
  static var stadiumConNo = "";
  static var stadiumNoCouet = "";
  static var stadiumResURL = "";
  static var stadiumImg;
  double stadiumLat = 0;
  double stadiumLon = 0;

  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

  //Validation variables
  bool _validateName = true;
  bool _validatePhoneNum = true;
  bool _validateNumCourt = true;
  bool _validateURL = true;
  static String messageName = "Error in text";
  static String messagePhoneNum = "Error in text";
  static String messageNumCourt = "Error in text";
  static String messageURL = "Error in text";

  // DISABLED BTN--------------------------
  void checkEnabled() {
    if ((_nameFieldController.text.toString() != '' &&
        _validateName == true &&
        _validatePhoneNum == true &&
        _validateNumCourt == true &&
        _validateURL == true)) {
      setState(() {
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

//Save values to db
  saveValues(List<String> paramList) async {
    if (_nameFieldController.text != "" &&
        _validateName == true &&
        _validatePhoneNum == true &&
        _validateNumCourt == true &&
        _validateURL == true) {
      if (pickedImage != null) {
        await saveImage(pickedImage); // for save image
      } else {
        if (_imageFilePath == null) {
          await _deleteOldImage(deleteImage);
        }
      }

      phonNumber = _currentcuntrycode + '-' + _phonenumFieldController.text;

      setState(() => newData[0] = _nameFieldController.text);
      setState(() => newData[1] = _adressFieldController.text);
      setState(() => newData[2] = phonNumber);
      setState(() => newData[3] = _nocourtFieldController.text);
      setState(() => newData[4] = _reservationFieldController.text);
      setState(() => newData[5] = _imageFilePath);
      setState(() => newData[6] = _lat);
      setState(() => newData[7] = _long);
      setState(() => newData[8] = stadiumId);
      print(stadiumId);
      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, param);
    } else {}
  }

  @override
  void initState() {
    super.initState();

    if (widget.arguments['stadiumId'].toString() != "") {
      setState(() {
        stadiumId = widget.arguments['stadiumId'].toString();
        stdName = widget.arguments['stadiumName'].toString();
        stadiumAddress = widget.arguments['stadiumAddress'].toString();
        stadiumConNo = widget.arguments['contactNumber'].toString();
        stadiumNoCouet = widget.arguments['noOfCourts'].toString();
        stadiumResURL = widget.arguments['reservationUrl'].toString();
        stadiumImg = widget.arguments['stadiumImage'];
        stadiumLat = widget.arguments['stadiumLat'];
        stadiumLon = widget.arguments['stadiumLon'];
      });

//set data to the screen
      var contryCode = stadiumConNo.split('-');

      _nameFieldController.text = stdName;
      _adressFieldController.text = stadiumAddress;
      _reservationFieldController.text = stadiumResURL;
      _phonenumFieldController.text = contryCode[1];
      _nocourtFieldController.text = stadiumNoCouet;

      _dropDownMenuItems = getDropDownMenuItems();
      if (contryCode[0] == "" || contryCode[0] == null) {
        _currentcuntrycode = _dropDownMenuItems[0].value;
      } else {
        _currentcuntrycode = contryCode[0];
      }
    }

    _imageFilePath = stadiumImg;
    deleteImage = stadiumImg;
    _lat = stadiumLat;
    _long = stadiumLon;
    _city = stadiumAddress;

    checkEnabled();
    controller = new BaseController(this);

    checkImage(stadiumImg);
    checkLocation(stadiumLat, stadiumLon);
  }

  //Check image availaibilty
  void checkImage(stadiumImg) {
    if (stadiumImg != null) {
      setState(() {
        _imageAvailability = checkImageAvailability(File(stadiumImg));
      });
    } else {
      setState(() {
        _imageAvailability = false;
      });
    }
  }

  //check map location is set
  void checkLocation(stadiumLat, stadiumLon) {
    if (stadiumLat != 0 && stadiumLon != 0) {
      setState(() {
        _mapLocation = LatLng(stadiumLat, stadiumLon);
        _mapDisplay = true;
      });
    } else {
      setState(() {
        _mapDisplay = false;
      });
    }
  }

  //loading map
  void _onMapCreated(GoogleMapController mcontroller) {
    mapController = mcontroller;
    markers.clear();
    _addMarker();
  }

  //adding maker to map
  void _addMarker() {
    openMap(LatLng _loc) => {
          controller.execFunction(
              ControllerFunc.launch_app,
              ControllerSubFunc.launch_map,
              {'lat': _loc.latitude, 'long': _loc.longitude}),
        };
    final String _markerIdVal = 'marker';
    final MarkerId _markerId = MarkerId(_markerIdVal);

    final Marker marker = Marker(
        markerId: _markerId,
        position: _mapLocation,
        consumeTapEvents: true,
        onTap: () => openMap(_mapLocation),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed));

    setState(() {
      markers[_markerId] = marker;
    });
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String countryCode in _countryCode) {
      items.add(
        new DropdownMenuItem(
          value: countryCode,
          child: new Text(countryCode),
        ),
      );
    }
    return items;
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 190.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else {}
  }

//image remove/edit popup functions
  void choiceAction(choice) async {
    // print(choice);
    if (choice == "Take") {
      var img = await pickImageFromCamGallery(ImageSource.camera);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Choose") {
      var img = await pickImageFromCamGallery(ImageSource.gallery);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Remove") {
      removeImage();
    }
  }

  //for map
  _onFromTextFiledTap() async {
    places.Prediction p = await PlacesAutocomplete.show(
      logo: Row(),
      context: context,
      apiKey: Const.mapsAPIKey,
      mode: Mode.overlay,
      // language: "lk",
      // components: [places.Component(places.Component.country, "lk")],
    );
    if (p != null) {
      places.PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      setState(() {
        _lat = detail.result.geometry.location.lat;
        _long = detail.result.geometry.location.lng;
        _city = detail.result.formattedAddress;
        _mapLocation = LatLng(_lat, _long);
      });

      if (!_mapDisplay) {
        setState(() {
          _mapDisplay = true;
        });
      } else {
        markers.clear();
        _addMarker();
        mapController.animateCamera(
          CameraUpdate.newLatLngZoom(
            _mapLocation,
            14.0,
          ),
        );
      }

      _adressFieldController.text = _city;
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Container(
                                    child: Image.file(
                                      snapshot.data,
                                      fit: BoxFit.cover,
                                      width: double.infinity,
                                      height: 200.0,
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Container(
                                    height: 200.0,
                                    child: Container(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(top: 0.0),
                                        child: Container(
                                          width: double.infinity,
                                          height: 200,
                                          decoration: BoxDecoration(
                                            color: AppColors.gray,
                                            image: _imageAvailability
                                                ? DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: getImageProvider(
                                                        File(stadiumImg)))
                                                : null,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),
//cover of the image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 35.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: new FileImage(snapshot.data),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'Choose',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons.camera,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text(
                                                                'Choose Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      pickedImage != null
                                                          ? PopupMenuItem<
                                                                  String>(
                                                              value: 'Remove',
                                                              child: SizedBox
                                                                  .expand(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              10.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .remove_circle,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        'Remove Image'),
                                                                  ],
                                                                ),
                                                              ))
                                                          : null,
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Padding(
                                    padding: const EdgeInsets.only(top: 35.0),
                                    child: Container(
                                      width: 130.0,
                                      height: 130.0,
                                      decoration: new BoxDecoration(
                                        color: AppColors.white,
                                        shape: BoxShape.circle,
                                        image: new DecorationImage(
                                          fit: BoxFit.cover,
                                          image: stadiumImg != null
                                              ? getImageProvider(
                                                  File(stadiumImg))
                                              : new AssetImage(
                                                  "images/tennis-court.png"),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: SizedBox(
                                                  height: 38,
                                                  width: 38,
                                                  child: PopupMenuButton(
                                                    onSelected: choiceAction,
                                                    child: Container(
                                                      height: 40.0,
                                                      width: 40.0,
                                                      decoration: BoxDecoration(
                                                        color:
                                                            AppStyle.color_Head,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Icon(
                                                        Icons.camera_alt,
                                                        color: Colors.black,
                                                        size: 28,
                                                      ),
                                                    ),
                                                    itemBuilder: (BuildContext
                                                            context) =>
                                                        <PopupMenuItem<String>>[
                                                      PopupMenuItem<String>(
                                                        value: 'Take',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons
                                                                    .camera_enhance,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text('Take Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      PopupMenuItem<String>(
                                                        value: 'Choose',
                                                        child: SizedBox.expand(
                                                            child: Row(
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          10.0),
                                                              child: Icon(
                                                                Icons.camera,
                                                                size: 20,
                                                              ),
                                                            ),
                                                            Text(
                                                                'Choose Image'),
                                                          ],
                                                        )),
                                                      ),
                                                      _imageFilePath != null
                                                          ? PopupMenuItem<
                                                                  String>(
                                                              value: 'Remove',
                                                              child: SizedBox
                                                                  .expand(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          right:
                                                                              10.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .remove_circle,
                                                                        size:
                                                                            20,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        'Remove Image'),
                                                                  ],
                                                                ),
                                                              ))
                                                          : null,
                                                    ],
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),

//profile name----------------------------
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 165.0, left: 20, right: 20),
                          child: Container(
                            height: 30.0,
                            child: Text(
                              stdName,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
//name--------------------------------
                  new Container(
                    width: 300.0,
                    child: new Text(
                      'Name',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 25,
                        cursorColor: Colors.black,
                        controller: _nameFieldController,
                        onChanged: (value) {
                          setState(() {
                            stdName = value;
                          });

                          var errorStatus = TextBoxValidation.isEmpty(
                              _nameFieldController.text);

                          setState(() {
                            _validateName = errorStatus['state'];
                            messageName = errorStatus['errorMessage'];
                          });

                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          hintText: 'Enter Stadium Name',
                          counterText: "",
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _nameFieldController.clear();
                              stdName = "";

                              var errorStatus = TextBoxValidation.isEmpty(
                                  _nameFieldController.text);

                              setState(() {
                                _validateName = errorStatus['state'];
                                messageName = errorStatus['errorMessage'];
                              });

                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateName ? null : messageName,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

//Address-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Address',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        cursorColor: Colors.black,
                        controller: _adressFieldController,
                        onChanged: (String newValue) {
                          checkEnabled();
                        },
                        onTap: () {
                          _onFromTextFiledTap();
                        },
                        decoration: new InputDecoration(
                          hintText: 'Enter Stadium Address',
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _adressFieldController.clear();
                              checkEnabled();

                              setState(() {
                                _lat = 0;
                                _long = 0;
                                _mapDisplay = false;
                              });
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

// contact no-------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Contact NO',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 0.0, bottom: 8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  height: 48,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: AppColors.form_border,
                                      style: BorderStyle.solid,
                                      width: 1.2,
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Positioned(
                                        right: 0,
                                        top: 0,
                                        child: Container(
                                          width: 50,
                                          height: 48,
                                          child: Center(
                                            child: Icon(Icons.expand_more,
                                                size: 24, color: Colors.black),
                                          ),
                                        ),
                                      ),
                                      Theme(
                                        data: Theme.of(context).copyWith(),
                                        child: DropdownButtonHideUnderline(
                                          child: Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: new DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              value: _currentcuntrycode,
                                              items: _dropDownMenuItems,
                                              onChanged:
                                                  countryCodeDropDownItem,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Rajdhani'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),

// phone number--------------------------------
                            Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 20.0, top: 0),
                                  child: new Container(
                                    width: 180,
                                    child: new TextField(
                                      cursorColor: Colors.black,
                                      maxLength: 14,
                                      keyboardType: TextInputType.number,
                                      controller: _phonenumFieldController,
                                      onChanged: (String newValue) {
                                        if (_phonenumFieldController.text !=
                                            "") {
                                          var errorStatus =
                                              TextBoxValidation.isInteger(
                                                  _phonenumFieldController
                                                      .text);

                                          setState(() {
                                            _validatePhoneNum =
                                                errorStatus['state'];
                                            messagePhoneNum =
                                                errorStatus['errorMessage'];
                                          });
                                        } else {
                                          setState(() {
                                            _validatePhoneNum = true;
                                          });
                                        }
                                        checkEnabled();
                                      },
                                      decoration: new InputDecoration(
                                        counterText: "",
                                        hintText: 'Enter Contact Number',
                                        filled: true,
                                        fillColor: AppColors.backgroundColor,
                                        suffixIcon: IconButton(
                                          iconSize: 18,
                                          color: Colors.black,
                                          icon: Icon(Icons.close),
                                          onPressed: () {
                                            _phonenumFieldController.clear();
                                            setState(() {
                                              _validatePhoneNum = true;
                                            });
                                            checkEnabled();
                                          },
                                        ),
                                        contentPadding:
                                            EdgeInsets.only(left: 10.0),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.form_border,
                                              width: 1.2),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.form_border,
                                              width: 1.2),
                                        ),
                                        errorText: _validatePhoneNum
                                            ? null
                                            : messagePhoneNum,
                                        errorBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4)),
                                            borderSide: BorderSide(
                                                width: 1.2, color: Colors.red)),
                                        focusedErrorBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4)),
                                            borderSide: BorderSide(
                                                width: 1.2, color: Colors.red)),
                                      ),
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),

//No of Courts-----------------------------------

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'No of Courts',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 0.0, bottom: 10.0),
                    child: Container(
                      width: 300,
                      // height: 48,
                      child: new TextField(
                        cursorColor: Colors.black,
                        keyboardType: TextInputType.number,
                        controller: _nocourtFieldController,
                        onChanged: (String newValue) {
                          if (_nocourtFieldController.text != "") {
                            var errorStatus = TextBoxValidation.isInteger(
                                _nocourtFieldController.text);

                            setState(() {
                              _validateNumCourt = errorStatus['state'];
                              messageNumCourt = errorStatus['errorMessage'];
                            });
                          } else {
                            setState(() {
                              _validateNumCourt = true;
                            });
                          }

                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          hintText: 'Enter No of Courts',
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _nocourtFieldController.clear();
                              setState(() {
                                _validateNumCourt = true;
                              });
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateNumCourt ? null : messageNumCourt,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

//Reservation URL---------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Reservation URL',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        cursorColor: Colors.black,
                        controller: _reservationFieldController,
                        onChanged: (String newValue) {
                          if (_reservationFieldController.text != "") {
                            var errorStatus = TextBoxValidation.isUrlmatch(
                                _reservationFieldController.text);

                            setState(() {
                              _validateURL = errorStatus['state'];
                              messageURL = errorStatus['errorMessage'];
                            });
                          } else {
                            setState(() {
                              _validateURL = true;
                            });
                          }
                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          hintText: 'Enter Reservation URL',
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _reservationFieldController.clear();
                              setState(() {
                                _validateURL = true;
                              });
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.form_border, width: 1.2),
                          ),
                          errorText: _validateURL ? null : messageURL,
                          errorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1.2, color: Colors.red)),
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),

//GPS Location---------------------------
                  _mapDisplay
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: new Container(
                            width: 300.0,
                            child: new Text(
                              'Map',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  _mapDisplay
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                            width: 300,
                            height: 300,
                            child: GoogleMap(
                              onMapCreated: _onMapCreated,
                              initialCameraPosition: CameraPosition(
                                target: _mapLocation,
                                zoom: 14,
                              ),
                              markers: Set<Marker>.of(markers.values),
                              zoomGesturesEnabled: false,
                              myLocationEnabled: false,
                              tiltGesturesEnabled: false,
                              rotateGesturesEnabled: false,
                              scrollGesturesEnabled: false,
                              compassEnabled: false,
                              myLocationButtonEnabled: false,
                            ),
                          ),
                        )
                      : Container(),
//Save button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: disabledBtn,
                          onPressed: (_nameFieldController.text != "" &&
                                  _validateName == true &&
                                  _validatePhoneNum == true &&
                                  _validateNumCourt == true &&
                                  _validateURL == true)
                              ? () => showDialog(
                                  context: context,
                                  builder: (context) => CommonPopup(
                                      context, "edit", [], saveValues))
                              : () {},
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: disabledBtnFont,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "Save",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

//country code selection
  void countryCodeDropDownItem(String selectedcountrycode) {
    setState(() {
      _currentcuntrycode = selectedcountrycode;
    });
  }

  //Open gallery
  pickImageFromCamGallery(ImageSource source) async {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
    return imageFile;
  }

  //Save image to gallery
  saveImage(img) async {
    final directory = await getApplicationDocumentsDirectory();
    // final String pathx = directory.path;
    final String pathx = '${directory.path}/tennis/pictures'; //new directory
    await Directory(pathx).create(recursive: true);

    String path = img.path;
    if (path != "" && path != null) {
      var savedFile = File.fromUri(Uri.file(path));

      _deleteOldImage(deleteImage); //delete old image
      var randomNumber = Uuid().v1();
      var iName = randomNumber.toString() + ".png"; //for image

      final imagePath = '$pathx/$iName';
      File newImageFile = File(imagePath);
      var finalSavedFile =
          await newImageFile.writeAsBytes(await savedFile.readAsBytes());

      setState(() {
        _imageFilePath = finalSavedFile.path;
      });
    }
  }

  //Remove image
  removeImage() {
    setState(() {
      imageFile = null; //remove both images, When image recently selected,
      stadiumImg = null; // for player imge
      _imageAvailability = false; //for bg image
      _imageFilePath = null; //DB
      pickedImage = null; //for delte
    });
  }

  //delete image from directory
  _deleteOldImage(stdImage) async {
    if (stdImage != null) {
      final delFir = File(stdImage);
      if (delFir.existsSync()) {
        delFir.deleteSync(recursive: true); //for delete
      }
    }
  }

  // if file not found set default
  ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(f)
        : const AssetImage("images/tennis-court.png");
  }

  //check available for set background image
  bool checkImageAvailability(File f) {
    return f.existsSync() ? true : false;
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            if (response['response_data'].toString() != "null") {
              setState(() {
                if (response['response_data'].toString() != "[]") {
                  sqlData = response['response_data'].toString();
                } else {
                  sqlData = "null";
                }
              });
            }

            widget.onSelected(RoutingData.Stadium, true, false);
          }
          break;
        }
      default:
        {
          //Do nothing
        }
    }
  }
}
