import 'dart:ui';
import 'dart:io';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:tenizo/util/player_registration_util.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/validation/textBoxValidation.dart';
import 'package:uuid/uuid.dart';
import 'package:path_provider/path_provider.dart';
import 'package:camera/camera.dart';

class PlayerRegistration extends StatefulWidget {
  final Function onSelected;
  final String userName;
  @override
  _PlayerRegistrationState createState() => _PlayerRegistrationState();
  PlayerRegistration({Key key, this.onSelected, this.userName})
      : super(key: key);
}

class _PlayerRegistrationState extends State<PlayerRegistration>
    with BaseControllerListner {
  String sqlData, objectDbData;

  BaseController controller;
  TextEditingController _nameFieldController = TextEditingController();
  TextEditingController _teamFieldController = TextEditingController();

  Future<File> imageFile;
  // File imageFile;
  File pickedImage;
  var playerid;
  String playerNo = "";

  List _gender = [
    CommonValues.notSet,
    CommonValues.genderMale,
    CommonValues.genderFemale
  ];
  List _hand = [
    CommonValues.notSet,
    CommonValues.unknown,
    CommonValues.rightHanded,
    CommonValues.leftHanded,
    CommonValues.doubleHanded
  ];
  List _playStyle = [
    CommonValues.notSet,
    CommonValues.baseline,
    CommonValues.attackTheNet,
    CommonValues.counterPunching,
    CommonValues.allCourt
  ];
  List _role = [
    CommonValues.notSet,
    CommonValues.roleKey,
    CommonValues.roleOther
  ];

  List<DropdownMenuItem<String>> _dropDownMenuItemsGender;
  List<DropdownMenuItem<String>> _dropDownMenuItemsHand;
  List<DropdownMenuItem<String>> _dropDownMenuItemsPlayStyle;
  List<DropdownMenuItem<String>> _dropDownMenuItemsRole;

  static var newData = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11'
  ];

  var param = [
    "INSERT INTO player(user_name,player_id,Player_no, name, gender, date_of_birth, handedness, playstyle ,team, role, player_image, status) VALUES (?,? ,?, ? , ? , ?, ? , ? , ?, ?, ?, ?)",
    newData,
    {"calledMethod": "insertPlayer"}
  ];

  var param2 = [
    "SELECT * FROM player where status =?",
    [0]
  ];

  static var dateObject = DateTime.now();
  static var today = DateTime.now();

  String displayedDate = dateFormatter(dateObject);
  String name = "";
  String _imageFilePath;
  bool isenable;

  //DISABLED BUTTON
  static var disabledBtn = AppColors.gray;
  static var disabledBtnFont = AppColors.white;

//Validation variables
  bool _validateName = true;
  static String messageName = "Error in text";

  //DISABLED BUTTON
  void checkEnabled() {
    if ((_nameFieldController.text.toString() != '') && _validateName == true) {
      setState(() {
        disabledBtn = AppColors.ternary_color;
        disabledBtnFont = AppColors.black;
      });
    } else {
      setState(() {
        disabledBtn = AppColors.gray;
        disabledBtnFont = AppColors.white;
      });
    }
  }

  //load player number
  _loadPlayerNumber() {
    var selectQuery = [
      "SELECT Player_no from player where user_name = ? ORDER BY Player_no DESC",
      [widget.userName],
      {"calledMethod": "selectPlayerNumber"}
    ];
    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, selectQuery);
  }

//SAVE TO DATABASE
  saveValues() async {
    if (_nameFieldController.text != '' && _validateName == true) {
      if (pickedImage != null) {
        await saveImage(pickedImage); // for save image
      }
      playerid = Uuid().v1();

      setState(() => newData[0] = widget.userName); //user_name
      setState(() => newData[1] = playerid); //player_id
      setState(() => newData[2] = playerNo); //Player_no
      setState(() => newData[3] = _nameFieldController.text); //name
      setState(() => newData[4] = _currentGender); //gender
      setState(() => newData[5] = displayedDate); //date_of_birth
      setState(() => newData[6] = _currentHandedness); //handedness
      setState(() => newData[7] = _currentPlayStyle); //playstyle
      setState(() => newData[8] = _teamFieldController.text); //team
      setState(() => newData[9] = _currentRole); //role
      setState(() => newData[10] = _imageFilePath); //player_image
      setState(() => newData[11] = '0'); //status

      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_insert, param);
    } else {}
  }

  String _currentGender;
  String _currentHandedness;
  String _currentPlayStyle;
  String _currentRole;

  @override
  void initState() {
    super.initState();
    checkEnabled();

    _dropDownMenuItemsGender = getDropDownMenuItemsGender();
    _currentGender = _dropDownMenuItemsGender[0].value;

    _dropDownMenuItemsHand = getDropDownMenuItemsHand();
    _currentHandedness = _dropDownMenuItemsHand[0].value;

    _dropDownMenuItemsPlayStyle = getDropDownMenuItemsPlayStyle();
    _currentPlayStyle = _dropDownMenuItemsPlayStyle[0].value;

    _dropDownMenuItemsRole = getDropDownMenuItemsRole();
    _currentRole = _dropDownMenuItemsRole[1].value;

    controller = new BaseController(this);
    _loadPlayerNumber();
  }

  static dateFormatter(date) {
    var selectedyear = date.year;
    var selectedmonth = 'Jan';
    var selectedDate = date.day;
    switch (date.month) {
      case 1:
        selectedmonth = 'Jan';
        break;
      case 2:
        selectedmonth = 'Feb';
        break;
      case 3:
        selectedmonth = 'Mar';
        break;
      case 4:
        selectedmonth = 'Apr';
        break;
      case 5:
        selectedmonth = 'May';
        break;
      case 6:
        selectedmonth = 'Jun';
        break;
      case 7:
        selectedmonth = 'Jul';
        break;
      case 8:
        selectedmonth = 'Aug';
        break;
      case 9:
        selectedmonth = 'Sept';
        break;
      case 10:
        selectedmonth = 'Oct';
        break;
      case 11:
        selectedmonth = 'Nov';
        break;
      case 12:
        selectedmonth = 'Dec';
        break;
      default:
    }
    return '$selectedmonth $selectedDate , $selectedyear';
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsGender() {
    List<DropdownMenuItem<String>> items = new List();
    for (String gender in _gender) {
      items.add(new DropdownMenuItem(value: gender, child: new Text(gender)));
    }
    return items;
  }

  customFont(text) {
    return Text(text, textAlign: TextAlign.left, style: customFontStyle());
  }

  customFontStyle() {
    return TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  pNumberFontStyle() {
    return TextStyle(
        fontSize: 18,
        height: 2.7,
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  customFontTopic(text) {
    return Text(
      text,
      textAlign: TextAlign.left,
      style: TextStyle(
          fontSize: 18,
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontFamily: 'Rajdhani'),
    );
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsHand() {
    List<DropdownMenuItem<String>> items = new List();
    for (String hand in _hand) {
      items.add(new DropdownMenuItem(value: hand, child: new Text(hand)));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsPlayStyle() {
    List<DropdownMenuItem<String>> items = new List();
    for (String style in _playStyle) {
      items.add(new DropdownMenuItem(value: style, child: new Text(style)));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsRole() {
    List<DropdownMenuItem<String>> items = new List();
    for (String role in _role) {
      items.add(new DropdownMenuItem(value: role, child: new Text(role)));
    }
    return items;
  }

  static var deviceWidth = 0.0;
  static var deviceHeight = 0.0;
  var paddingData = const EdgeInsets.only(left: 210.0, top: 120.0);

  _setDeviceWidth() {
    if (deviceWidth <= 350) {
      paddingData = const EdgeInsets.only(left: 180.0, top: 130.0);
    } else if (deviceWidth <= 400) {
      paddingData = const EdgeInsets.only(left: 200.0, top: 130.0);
    } else if (deviceWidth <= 450) {
      paddingData = const EdgeInsets.only(left: 230.0, top: 130.0);
    } else if (deviceWidth <= 500) {
      paddingData = const EdgeInsets.only(left: 225.0, top: 130.0);
    } else if (deviceWidth <= 550) {
      paddingData = const EdgeInsets.only(left: 225.0, top: 130.0);
    } else {}
  }

  //image remove/edit popup functions
  void choiceAction(choice) async {
    if (choice == "Take") {
      var img = await pickImageFromCamGallery(ImageSource.camera);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Choose") {
      var img = await pickImageFromCamGallery(ImageSource.gallery);
      setState(() {
        pickedImage = img;
      });
    } else if (choice == "Remove") {
      setState(() {
        imageFile = null;
        _imageFilePath = null;
        pickedImage = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      deviceWidth = MediaQuery.of(context).size.width;
      deviceHeight = MediaQuery.of(context).size.height;
    });
    _setDeviceWidth();

    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            // color: AppColors.white,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
//Background image ----------------------------------------------
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: FutureBuilder<File>(
                              future: imageFile,
                              builder: (BuildContext context,
                                  AsyncSnapshot<File> snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.data != null) {
                                  return Container(
                                    child: Image.file(
                                      snapshot.data,
                                      fit: BoxFit.cover,
                                      width: double.infinity,
                                      height: 200.0,
                                    ),
                                  );
                                } else if (snapshot.error != null) {
                                  return const Text(
                                    'Error Picking Image',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 25,
                                      color: Colors.white,
                                      decoration: TextDecoration.underline,
                                    ),
                                  );
                                } else {
                                  return Container(
                                    height: 200.0,
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),
//Player profile image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: FutureBuilder<File>(
                                future: imageFile,
                                builder: (BuildContext context,
                                    AsyncSnapshot<File> snapshot) {
                                  if (snapshot.connectionState ==
                                          ConnectionState.done &&
                                      snapshot.data != null) {
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 30.0),
                                      child: Container(
                                        width: 130.0,
                                        height: 130.0,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                            fit: BoxFit.cover,
                                            image: new FileImage(snapshot.data),
                                          ),
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 3.0,
                                          ),
                                        ),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Align(
                                                  alignment:
                                                      Alignment.bottomRight,
                                                  child: SizedBox(
                                                    height: 38,
                                                    width: 38,
                                                    child: PopupMenuButton(
                                                      onSelected: choiceAction,
                                                      child: Container(
                                                        height: 40.0,
                                                        width: 40.0,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: AppStyle
                                                              .color_Head,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        child: Icon(
                                                          Icons.camera_alt,
                                                          color: Colors.black,
                                                          size: 28,
                                                        ),
                                                      ),
                                                      itemBuilder: (BuildContext
                                                              context) =>
                                                          <
                                                              PopupMenuItem<
                                                                  String>>[
                                                        PopupMenuItem<String>(
                                                          value: 'Take',
                                                          child:
                                                              SizedBox.expand(
                                                                  child: Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            10.0),
                                                                child: Icon(
                                                                  Icons
                                                                      .camera_enhance,
                                                                  size: 20,
                                                                ),
                                                              ),
                                                              Text(
                                                                  'Take Image'),
                                                            ],
                                                          )),
                                                        ),
                                                        PopupMenuItem<String>(
                                                            value: 'Choose',
                                                            child:
                                                                SizedBox.expand(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            10.0),
                                                                    child: Icon(
                                                                      Icons
                                                                          .camera,
                                                                      size: 20,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                      'Choose Image'),
                                                                ],
                                                              ),
                                                            )),
                                                        pickedImage != null
                                                            ? PopupMenuItem<
                                                                    String>(
                                                                value: 'Remove',
                                                                child: SizedBox
                                                                    .expand(
                                                                  child: Row(
                                                                    children: <
                                                                        Widget>[
                                                                      Padding(
                                                                        padding:
                                                                            const EdgeInsets.only(right: 10.0),
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .remove_circle,
                                                                          size:
                                                                              20,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                          'Remove Image'),
                                                                    ],
                                                                  ),
                                                                ))
                                                            : null,
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  } else if (snapshot.error != null) {
                                    return const Text(
                                      'Error Picking Image',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 25,
                                        color: Colors.white,
                                        decoration: TextDecoration.underline,
                                      ),
                                    );
                                  } else {
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 30.0),
                                      child: Container(
                                        width: 130.0,
                                        height: 130.0,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: new DecorationImage(
                                            fit: BoxFit.fill,
                                            image: new AssetImage(
                                                "images/user.png"),
                                          ),
                                          border: Border.all(
                                            color: Colors.black,
                                            width: 3.0,
                                          ),
                                        ),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Align(
                                                  alignment:
                                                      Alignment.bottomRight,
                                                  child: SizedBox(
                                                    height: 38,
                                                    width: 38,
                                                    child: PopupMenuButton(
                                                      onSelected: choiceAction,
                                                      child: Container(
                                                        height: 40.0,
                                                        width: 40.0,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: AppStyle
                                                              .color_Head,
                                                          shape:
                                                              BoxShape.circle,
                                                        ),
                                                        child: Icon(
                                                          Icons.camera_alt,
                                                          color: Colors.black,
                                                          size: 28,
                                                        ),
                                                      ),
                                                      itemBuilder: (BuildContext
                                                              context) =>
                                                          <
                                                              PopupMenuItem<
                                                                  String>>[
                                                        PopupMenuItem<String>(
                                                          value: 'Take',
                                                          child:
                                                              SizedBox.expand(
                                                                  child: Row(
                                                            children: <Widget>[
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            10.0),
                                                                child: Icon(
                                                                  Icons
                                                                      .camera_enhance,
                                                                  size: 20,
                                                                ),
                                                              ),
                                                              Text(
                                                                  'Take Image'),
                                                            ],
                                                          )),
                                                        ),
                                                        PopupMenuItem<String>(
                                                            value: 'Choose',
                                                            child:
                                                                SizedBox.expand(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        right:
                                                                            10.0),
                                                                    child: Icon(
                                                                      Icons
                                                                          .camera,
                                                                      size: 20,
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                      'Choose Image'),
                                                                ],
                                                              ),
                                                            )),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
//profile name----------------------------
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 175.0),
                          child: Container(
                            height: 30.0,
                            child: Text(
                              playerNo + " - " + name,
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 16.0,
                  ),

// Name ----------------------------------------------
                  new Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Name'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      0.0,
                      0.0,
                      0.0,
                      8.0,
                    ),
                    child: new Container(
                      width: 300.0,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 5.0),
                            child: Text(
                              playerNo + " - ",
                              style: customFontStyle(),
                            ),
                          ),
                          Flexible(
                            child: TextField(
                              maxLength: 25,
                              controller: _nameFieldController,
                              onChanged: (value) {
                                setState(() {
                                  name = value;
                                });

                                var errorStatus = TextBoxValidation.isEmpty(
                                    _nameFieldController.text);

                                setState(() {
                                  _validateName = errorStatus['state'];
                                  messageName = errorStatus['errorMessage'];
                                });

                                checkEnabled();
                              },
                              decoration: new InputDecoration(
                                counterText: "",
                                hintText: 'Enter player name ...',
                                filled: true,
                                fillColor: AppColors.backgroundColor,
                                suffixIcon: IconButton(
                                  iconSize: 18,
                                  color: Colors.black,
                                  icon: Icon(Icons.close),
                                  onPressed: () {
                                    _nameFieldController.clear();
                                    name = "";

                                    var errorStatus = TextBoxValidation.isEmpty(
                                        _nameFieldController.text);

                                    if (errorStatus['state'] == false) {
                                      setState(() {
                                        _validateName = errorStatus['state'];
                                        messageName =
                                            errorStatus['errorMessage'];
                                      });
                                    }

                                    checkEnabled();
                                  },
                                ),
                                contentPadding: EdgeInsets.only(left: 10.0),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppStyle.color_Txt_Input_Border,
                                      width: 1.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppStyle.color_Txt_Input_Border,
                                      width: 1.0),
                                ),
                                errorText: _validateName ? null : messageName,
                                errorBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1.2, color: Colors.red)),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1.2, color: Colors.red)),
                              ),
                              style: customFontStyle(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  //Gender and birth date ----------------------------------------------
                  //Gender -------------------------------------------------------------
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 0.0, left: 0.0),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(right: 20.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                      padding: EdgeInsets.only(bottom: 8.0),
                                      child: customFontTopic('Gender')),
                                  new Container(
                                    height: 48,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: AppColors.form_border,
                                        style: BorderStyle.solid,
                                        width: 1.2,
                                      ),
                                    ),
                                    width: 110.0,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          right: 0,
                                          top: 0,
                                          child: Container(
                                            width: 30,
                                            height: 48,
                                            child: Center(
                                              child: Icon(Icons.expand_more,
                                                  size: 24,
                                                  color: Colors.black),
                                            ),
                                          ),
                                        ),
                                        Theme(
                                          data: Theme.of(context).copyWith(),
                                          child: DropdownButtonHideUnderline(
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: new DropdownButton(
                                                  isExpanded: true,
                                                  iconSize: 0,
                                                  value: _currentGender,
                                                  items:
                                                      _dropDownMenuItemsGender,
                                                  onChanged:
                                                      changedDropDownItemGender,
                                                  style: customFontStyle()),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            // Date of birth--------------------------------

                            Expanded(
                              child: Container(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(bottom: 8.0),
                                      child: customFontTopic('Date of Birth'),
                                    ),
                                    Container(
                                      width: double.infinity,
                                      height: 48,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(4.0),
                                        border: Border.all(
                                          color: AppColors.form_border,
                                          width: 1.2,
                                          style: BorderStyle.solid,
                                        ),
                                      ),
                                      child: FlatButton(
                                        onPressed: () {
                                          DatePicker.showDatePicker(context,
                                              showTitleActions: true,
                                              minTime: DateTime(1950, 1, 1),
                                              maxTime: DateTime(2025, 12, 31),
                                              onChanged: (date) {},
                                              onConfirm: (date) {
                                            PlayerRegistrationPageUtils
                                                .dateFormatter(date);
                                            setState(() {
                                              dateObject = date;
                                              displayedDate =
                                                  PlayerRegistrationPageUtils
                                                      .dateFormatter(date);
                                            });
                                          },
                                              currentTime: dateObject,
                                              locale: LocaleType.en);
                                        },
                                        child: customFont(displayedDate),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

                  //Handedness ------------------------------------------------------------

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 8.0),
                            child: customFontTopic('Handedness'),
                          ),
                          Container(
                            height: 48,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: AppColors.form_border,
                                style: BorderStyle.solid,
                                width: 1.2,
                              ),
                            ),
                            // width: 200,
                            child: Stack(
                              children: <Widget>[
                                Positioned(
                                  right: 0,
                                  top: 0,
                                  child: Container(
                                    width: 30,
                                    height: 48,
                                    child: Center(
                                      child: Icon(Icons.expand_more,
                                          size: 24, color: Colors.black),
                                    ),
                                  ),
                                ),
                                Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: DropdownButtonHideUnderline(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 8.0),
                                      child: new DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        value: _currentHandedness,
                                        items: _dropDownMenuItemsHand,
                                        onChanged: changedDropDownItemHand,
                                        style: customFontStyle(),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // Play Style-------------------------------------------------------------------

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(bottom: 8.0),
                            child: customFontTopic('Play Style'),
                          ),
                          Container(
                            height: 48,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                  color: AppStyle.color_Txt_Input_Border),
                            ),
                            // width: 200,
                            child: Stack(
                              children: <Widget>[
                                Positioned(
                                  right: 0,
                                  top: 0,
                                  child: Container(
                                    width: 30,
                                    height: 48,
                                    child: Center(
                                      child: Icon(Icons.expand_more,
                                          size: 24, color: Colors.black),
                                    ),
                                  ),
                                ),
                                Theme(
                                  data: Theme.of(context).copyWith(),
                                  child: DropdownButtonHideUnderline(
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: new DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        value: _currentPlayStyle,
                                        items: _dropDownMenuItemsPlayStyle,
                                        onChanged: changedDropDownItemPlayStyle,
                                        style: customFontStyle(),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  //Role ----------------------------------------------

                  Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Role'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                    child: new Container(
                      width: 300.0,
                      height: 48,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border:
                            Border.all(color: AppStyle.color_Txt_Input_Border),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Positioned(
                            right: 0,
                            top: 0,
                            child: Container(
                              width: 30,
                              height: 48,
                              child: Center(
                                child: Icon(Icons.expand_more,
                                    size: 24, color: Colors.black),
                              ),
                            ),
                          ),
                          Theme(
                            data: Theme.of(context).copyWith(),
                            child: new DropdownButtonHideUnderline(
                              child: Padding(
                                padding: EdgeInsets.only(left: 10.0),
                                child: new DropdownButton(
                                  isExpanded: true,
                                  iconSize: 0,
                                  value: _currentRole,
                                  items: _dropDownMenuItemsRole,
                                  onChanged: changedDropDownItemRole,
                                  style: customFontStyle(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  //Team ----------------------------------------------
                  new Container(
                    padding: EdgeInsets.only(bottom: 8.0),
                    width: 300.0,
                    child: customFontTopic('Team'),
                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      0.0,
                      0.0,
                      0.0,
                      8.0,
                    ),
                    child: new Container(
                      width: 300.0,
                      child: new TextField(
                        maxLength: 25,
                        controller: _teamFieldController,
                        onChanged: (String newValue) {
                          checkEnabled();
                        },
                        decoration: new InputDecoration(
                          counterText: "",
                          filled: true,
                          fillColor: AppColors.backgroundColor,
                          hintText: 'Enter team name ...',
                          suffixIcon: IconButton(
                            iconSize: 18,
                            color: Colors.black,
                            icon: Icon(Icons.close),
                            onPressed: () {
                              _teamFieldController.clear();
                              checkEnabled();
                            },
                          ),
                          contentPadding: EdgeInsets.only(left: 10.0),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppStyle.color_Txt_Input_Border,
                                width: 1.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppStyle.color_Txt_Input_Border,
                                width: 1.0),
                          ),
                        ),
                        style: customFontStyle(),
                      ),
                    ),
                  ),

                  //Save Button ----------------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 160,
                      height: 35,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: disabledBtn,
                          onPressed: () {
                            if (_nameFieldController.text != "" &&
                                _validateName == true) {
                              saveValues();
                            } else {
                              checkValidation();
                            }
                          },
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: disabledBtnFont,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "ADD",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Rajdhani',
                                        fontSize: 18),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

//Check the validation when click the button

  checkValidation() {
//Name ---------

    var errorStatus = TextBoxValidation.isEmpty(_nameFieldController.text);

    setState(() {
      _validateName = errorStatus['state'];
      messageName = errorStatus['errorMessage'];
    });
  }

  void changedDropDownItemGender(String selectedGender) {
    setState(() {
      _currentGender = selectedGender;
    });
  }

  void changedDropDownItemHand(String selectedHand) {
    setState(() {
      _currentHandedness = selectedHand;
    });
  }

  void changedDropDownItemPlayStyle(String selectedPlayStyle) {
    setState(() {
      _currentPlayStyle = selectedPlayStyle;
    });
  }

  void changedDropDownItemRole(String selectedRole) {
    setState(() {
      _currentRole = selectedRole;
    });
  }

  //Open gallery
  pickImageFromCamGallery(ImageSource source) async {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
    });
    // final directory = await getApplicationDocumentsDirectory();
    // // final String dirPath = ;
    // // await Directory(dirPath).create(recursive: true);
    // final String pathx = '${directory.path}/tennis/pictures';
    // // await Directory(pathx).create(recursive: true);
    // final dir = Directory(pathx);
    // // dir.deleteSync(recursive: true); //for delete

    // // final dir = Directory(pathx);

    // print("__________");
    // if (dir.existsSync()) {
    //   var files = dir.listSync().toList();
    //   files.forEach((e) => print(e.path));
    // }
    return imageFile;
  }

  //Save image to app directory
  saveImage(File img) async {
    final directory = await getApplicationDocumentsDirectory();
    // final String pathx = directory.path;
    final String pathx = '${directory.path}/tennis/pictures'; //new directory
    await Directory(pathx).create(recursive: true);

    String path = img.path;
    if (path != "" && path != null) {
      var savedFile = File.fromUri(Uri.file(path));

      // Method1-----------------
      print("----------");
      var randomNumber = Uuid().v1();
      var iName = randomNumber.toString() + ".png"; //for image

      final imagePath = '$pathx/$iName';
      File newImageFile = File(imagePath);
      var finalSavedFile =
          await newImageFile.writeAsBytes(await savedFile.readAsBytes());

      // Method2-----------------
      // var savedFile = File.fromUri(Uri.file(path));
      // final imagePath = '$pathx/jj.png';
      // File imageFile = File(imagePath);
      // // var finalSavedFile = await savedFile.copy(imagePath);
      // var finalSavedFile =
      //     await imageFile.writeAsBytes(await savedFile.readAsBytes());

      setState(() {
        _imageFilePath = finalSavedFile.path;
      });
    }
  }

  @override
  resultFunction(func, subFunc, response) {
    switch (func) {
      case ControllerFunc.db_sqlite:
        {
          if (response['response_state'] == true) {
            switch (response['calledMethod']) {
              case 'insertPlayer':
                if (response['response_data'].toString() != "null") {
                  setState(() {
                    if (response['response_data'].toString() != "[]") {
                      sqlData = response['response_data'].toString();
                    } else {
                      sqlData = "null";
                    }
                  });
                }
                widget.onSelected(RoutingData.Players, true, false);
                break;
              case 'selectPlayerNumber':
                if (response['response_data'].toString() != "null") {
                  if (response['response_data'].toString() != "[]") {
                    int x = response['response_data'][0]['Player_no'] + 1;
                    if (x < 10) {
                      setState(() {
                        playerNo = "0" + x.toString();
                      });
                    } else {
                      setState(() {
                        playerNo = x.toString();
                      });
                    }

                    // print(response['response_data'][0]['Player_no'].toString());S
                  } else {
                    setState(() {
                      playerNo = "01";
                    });
                  }
                }
                break;
              default:
            }
          }
        }
        break;

      default:
        {
          //Do nothing
        }
    }
  }
}
