import 'dart:io';
import 'dart:ui';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CourtView extends StatefulWidget {
  final arguments;
  final Function onSelected;

  @override
  _CourtViewState createState() => _CourtViewState();
  CourtView({Key key, this.onSelected, this.arguments}) : super(key: key);
}

class _CourtViewState extends State<CourtView> with BaseControllerListner {
  BaseController controller;

  Future<File> imageFile;
  bool _imageAvailability;

  static var courtId = "";
  static var courtName = "";
  static var courtSurface = "";
  static var courtIndoor = "";
  static var courtTime = "";
  static var courtImg;

  customFontStyle() {
    return TextStyle(
        fontSize: 18,
        color: Colors.black,
        fontWeight: FontWeight.w500,
        fontFamily: 'Rajdhani');
  }

  customFont(text) {
    return Text(text, textAlign: TextAlign.left, style: customFontStyle());
  }

  @override
  void initState() {
    super.initState();

    if (widget.arguments['stadiumId'].toString() != "") {
      setState(() {
        courtId = widget.arguments['courtId'].toString();
        courtName = widget.arguments['courtName'].toString();
        courtSurface = widget.arguments['courtSurface'].toString();
        courtIndoor = widget.arguments['courtIndoor'].toString();
        courtTime = widget.arguments['courtTime'].toString();
        courtImg = widget.arguments['courtImage'];
      });
    }
    controller = new BaseController(this);

    //Check image availaibilty
    if (courtImg != null) {
      setState(() {
        _imageAvailability = checkImageAvailability(File(courtImg));
      });
    } else {
      setState(() {
        _imageAvailability = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: MediaQuery(
        data: MediaQueryData(),
        child: Scaffold(
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
//backgorund image -------------------------
                      Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0.0),
                            child: Container(
                              width: double.infinity,
                              height: 200,
                              decoration: BoxDecoration(
                                color: AppColors.gray,
                                image: _imageAvailability
                                    ? DecorationImage(
                                        fit: BoxFit.cover,
                                        image: getImageProvider(File(courtImg)))
                                    : null,
                              ),
                            ),
                          ),
                        ),
                      ),
//Court image-----------------------------
                      Container(
                        color: Color.fromRGBO(0, 0, 0, 0.5),
                        height: 200.0,
                      ),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 35.0),
                          child: Container(
                            width: 130.0,
                            height: 130.0,
                            decoration: new BoxDecoration(
                              color: AppColors.white,
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: courtImg != null
                                    ? getImageProvider(File(courtImg))
                                    : new AssetImage("images/tennis-court.png"),
                              ),
                              // border: Border.all(
                              //   color: Colors.black,
                              //   width: 3.0,
                              // ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
//name--------------------------------
                  new Container(
                    width: 300.0,
                    child: new Text(
                      'Name / NO',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(courtName),
                    ),
                  ),

//Surface-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Court Surface',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(courtSurface),
                    ),
                  ),

// indoor-------------------------
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Indoor / Outdoor',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(courtIndoor),
                    ),
                  ),

//Available time-----------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Container(
                      width: 300.0,
                      child: new Text(
                        'Available Time',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      width: 300,
                      height: 48,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          color: AppColors.form_border,
                          style: BorderStyle.solid,
                          width: 1.2,
                        ),
                      ),
                      child: customFont(courtTime),
                    ),
                  ),

//OK button-------------------------------------

                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                    child: new Container(
                      width: 200,
                      height: 48,
                      margin: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
                      child: new RaisedButton(
                          padding:
                              EdgeInsets.only(top: 3.0, bottom: 3.0, left: 3.0),
                          color: AppColors.ternary_color,
                          onPressed: () {
                            widget.onSelected(RoutingData.Stadium, true, false);
                          },
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              side: BorderSide(
                                  color: Colors.black,
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              new Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 10.0),
                                  child: new Text(
                                    "OK",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 20),
                                  )),
                            ],
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onWillPop: () async => Future.value(false),
    );
  }

  // if file not found set default
  ImageProvider getImageProvider(File f) {
    return f.existsSync()
        ? FileImage(f)
        : const AssetImage("images/tennis-court.png");
  }

  //check available for set background image
  bool checkImageAvailability(File f) {
    return f.existsSync() ? true : false;
  }

  @override
  resultFunction(func, subFunc, response) {}
}
