import 'dart:async';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/rendering.dart';
import 'package:tenizo/layout/court_edit.dart';
import 'package:tenizo/layout/court_registration.dart';
import 'package:tenizo/layout/court_view.dart';
import 'package:tenizo/layout/edit_user_details.dart';
import 'package:tenizo/layout/help.dart';
// import 'package:tenizo/layout/privacy_policy.dart';
import 'package:tenizo/layout/stadium_edit.dart';
import 'package:tenizo/layout/stadium_view.dart';
import 'package:tenizo/layout/stats.dart';
import 'package:tenizo/layout/flow.dart';
// import 'package:tenizo/layout/terms_conditions.dart';
import 'package:tenizo/tennizo_base_controller.dart';
import 'package:tenizo/tennizo_controller_functions.dart';
import 'package:flutter/material.dart';
import 'package:tenizo/styles/app_style.dart';
import 'package:tenizo/layout/players.dart';
import 'package:tenizo/layout/stadium_list.dart';
import 'package:tenizo/layout/home.dart';
import 'package:tenizo/layout/gameSettings.dart';
import 'package:tenizo/layout/score.dart';
import 'package:tenizo/layout/stadium_registration.dart';
import 'package:tenizo/layout/player_registration.dart';
import 'package:tenizo/layout/view_player_details.dart';
import 'package:tenizo/layout/edit_player_details.dart';
import 'package:tenizo/layout/gameSettingUpdate.dart';
import 'package:tenizo/util/topbar.dart';
import 'package:tenizo/util/bottombar.dart';
import 'package:tenizo/util/app_const.dart';
import 'package:tenizo/packages/fancy_nav/fancy_bottom_navigation.dart';
import 'dart:ui' as ui;

const List<String> testDevice = [
  "B05320BDA238B998B796318604F7C069",
  '6AC6D5446CF52F292F234384B3765B45' //ARIT Android
  ,
  'C760B784A323890E461A67A643A2C861' //TAKUMI Android
  ,
  'AF5884AF8928469B76D8F0464C5C011F' //ARIT Android MacPC
  ,
  '761C3B80A2D15E19D48E3EF1B16B21E2',
  'B05320BDA238B998B796318604F7C069',
  '08267d5454eebb4eecdddc22bf78a528',
  '820AE456550795E0A81E17BA14188D31',
  '6C5C6EF4FDDA66E60E864C097B2D74E8',
  '2032D3D1C711A6901C9B52C4FD9480A6'
];

class MainPage extends StatefulWidget {
  final RoutingData selectedPage;
 // String userName;
  MainPage({Key key, this.selectedPage}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage>
    with BaseControllerListner, WidgetsBindingObserver {
  BaseController controller;
  GlobalKey bottomNavigationKey;
  int _selectedPage = RoutingData.Home.index;
  bool _showBottom = true;
  bool _showBackBtn = false;
  bool _showShareBtn = false;

  bool _showTopBar = true;
  bool showGameSettings = true;
  String customHeaderTitle = "TenniZo";
  int _flyPage = 0;
  GlobalKey screen = new GlobalKey();

  // Advertisements--------------------------------------------

  int _adType = 0;

  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    testDevices: testDevice != null ? testDevice : null,
    keywords: <String>['foo', 'bar'],
    contentUrl: 'http://foo.com/bar.html',
    childDirected: true,
    nonPersonalizedAds: true,
  );

  BannerAd _bannerAd;
  InterstitialAd _interstitialAd;
  int _coins = 0;
  BannerAd createBannerAd() {
    return BannerAd(
      adUnitId: BannerAd.testAdUnitId,
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        // print("BannerAd event $event");
      },
    );
  }


  InterstitialAd createInterstitialAd() {
    return InterstitialAd(
      adUnitId: 'ca-app-pub-2103257093190700/1270961321',
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        // print("InterstitialAd event $event");
        switch (event) {
          case MobileAdEvent.loaded:
            break;
          case MobileAdEvent.failedToLoad:
            initAd();
            if (_adType == 1) {
              screenShot();
            }
            _clearAdType();
            break;
          case MobileAdEvent.clicked:
            break;
          case MobileAdEvent.impression:
            break;
          case MobileAdEvent.opened:
            break;
          case MobileAdEvent.leftApplication:
            _clearAdType();
            initAd();
            break;
          case MobileAdEvent.closed:
            if (_adType == 1) {
              screenShot();
            }
            _clearAdType();
            initAd();
            break;
        }
      },
    );
  }

  _clearAdType() {
    setState(() {
      _adType = 0;
    });
  }
  //-----------------------------------

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();

    controller = new BaseController(this);

    bottomNavigationKey = GlobalKey();
    if (this._selectedPage < 4) {
      this._selectedPage = widget.selectedPage.index;
    } else {
      this._selectedPage = RoutingData.Home.index;
    }
    FirebaseAdMob.instance.initialize(appId: 'ca-app-pub-2103257093190700~9149451343');
    

    initAd();
  }

  initAd() {
    try {
      _interstitialAd?.dispose();
    } catch (e) {}
      _interstitialAd = createInterstitialAd()..load();
  }

  showAd(int _data) {
    if (_data != 0) {
      setState(() {
        _adType = _data;
      });
    }

    _interstitialAd?.show();
  }

  var arguments = {};
  onArgumentSaving(arg) {
    setState(() {
      arguments = arg;
    });
  }

  toggleNavBar() {
    setState(() {
      _showBottom = !_showBottom;
      _showTopBar = !_showTopBar;
    });
  }

  toggleStartState(bool showGameSettingsStatus) {
    setState(() {
      if (showGameSettingsStatus) {
        showGameSettings = true;
      } else {
        showGameSettings = false;
      }
    });
  }

  // SET THE POSITION OF THE BODY-----------------------------------------
  setPageIndex(int position) {
    setState(() {
      _selectedPage = position;
    });
    setHeaderTitle(RoutingData.values[position]);
  }

  // SET THE POSITION OF NAVBAR-----------------------------------------
  forceBottomNavChange(int position) {
    if (position < 4) {
      final FancyBottomNavigationState fState =
          bottomNavigationKey.currentState;
      fState.setPage(position);
    }
  }

  // SET THE TITLE OF THE HEADER-----------------------------------------
  setHeaderTitle(RoutingData position) {
    setState(() {
     _showShareBtn = false; 
    });
    String tempHeaderTitle = '';
    switch (position) {
      case RoutingData.Players:
        tempHeaderTitle = 'Players';
        break;

      case RoutingData.Stadium:
        tempHeaderTitle = 'Stadium';
        break;

      case RoutingData.Score:
        if (showGameSettings) {
          tempHeaderTitle = 'Game Settings';
          setState(() {
            _selectedPage = RoutingData.GameSettings.index;
          });
        } else {
          tempHeaderTitle = 'Score';
          setState(() {
            _selectedPage = RoutingData.Score.index;
          });
        }
        break;

      case RoutingData.Home:
        tempHeaderTitle = 'TenniZo';
        break;

      case RoutingData.GameSettings:
        tempHeaderTitle = 'Game Settings';
        break;

      case RoutingData.StadiumRegistration:
        tempHeaderTitle = 'Stadium Registration';
        break;

      case RoutingData.PlayerRegistration:
        tempHeaderTitle = 'Player Registration';
        break;

      case RoutingData.PlayerProfile:
        tempHeaderTitle = 'Player Profile';
        break;

      case RoutingData.StadiumDetails:
        tempHeaderTitle = 'Stadium Details';
        break;

      case RoutingData.StadiumEdit:
        tempHeaderTitle = 'Stadium Edit';
        break;

      case RoutingData.PlayerEdit:
        tempHeaderTitle = 'Edit Player';
        break;

      case RoutingData.CourtRegistration:
        tempHeaderTitle = 'Court Registration';
        break;

      case RoutingData.CourtView:
        tempHeaderTitle = 'Court Details';
        break;

      case RoutingData.CourtEdit:
        tempHeaderTitle = 'Court Edit';

        break;
      case RoutingData.Stats:
        setState(() {
          _showShareBtn = true; 
        });
        tempHeaderTitle = 'Stats';
        break;

      case RoutingData.Flow:
        tempHeaderTitle = 'Stats';
        break;

      case RoutingData.GameSettingUpdate:
        tempHeaderTitle = 'Game Setting Update';
        break;

      case RoutingData.EditUser:
        tempHeaderTitle = 'Edit User';
        break;

      case RoutingData.HelpPage:
        tempHeaderTitle = 'Help';
        break;

      default:
    }
    setState(() {
      customHeaderTitle = tempHeaderTitle;
    });
  }

  // BACK BUTTON FUNCTIONS--------------------------------------------------------
  onBackClicked(bool bottomBarShowHide, bool backArrowShowHide) {
    setPageIndex(_flyPage);
    showHideBottomBar(bottomBarShowHide);
    showHideHeaderBackArrow(backArrowShowHide);
  }

  //logout button onclick---------------------------------------------------------
  logoutButton() {
    var param1 = [
      "UPDATE user SET is_login = ? where is_login = ?",
      [0, 1],
      {"calledMethod": 'changeLogin'}
    ];

    controller.execFunction(
        ControllerFunc.db_sqlite, ControllerSubFunc.db_select, param1);
    Navigator.pushNamed(context, "/Login");
  }

  //Timer in main page -------------------

  String timeInNiceFormat = '00 : 00 : 00';
  int timeInSecondsMain = pauseGameTiem;
  var timeInSecondsFinal = '00 : 00 : 00';
  bool timerStatus = false;
  Timer _timer;
  final oneSec = Duration(seconds: 1);
  bool timerStatusMain = true;
  var currentMatchID;

  void togglePlayStateMain(
      timerStatus, timeInSecondsScore, palyStatus, matchId) {
    _timer?.cancel();
    _timer = null;

    timerStatusMain = palyStatus;

    setState(() {
      currentMatchID = matchId;
    });

    if (timerStatus) {
      timeFormatter(timeInSecondsScore);
      timeInSecondsMain = timeInSecondsScore;
      // Currently Playing but need to stop now
      timerStatus = false;
    } else {
      // Currently Stopped but need to run now
      _timer = Timer.periodic(
        oneSec,
        (Timer timer) => setState(
          () {
            timeInSecondsScore++;
            timeFormatter(timeInSecondsScore);
            timeInSecondsMain = timeInSecondsScore;
          },
        ),
      );

      timerStatus = true;
    }
  }

  timeFormatter(timeInSecondsScore) {
    if (timeInSecondsScore == null) {
      timeInSecondsScore = 0;
    }
    int time = timeInSecondsScore.toInt();
    int secs = time % 60;
    int mins = ((time / 60) % 60).toInt();
    int hrs = time ~/ 3600;

    String finalTime = '';

    if (hrs < 10) {
      finalTime += '0$hrs : ';
    } else {
      finalTime += '$hrs : ';
    }

    if (mins < 10) {
      finalTime += '0$mins : ';
    } else {
      finalTime += '$mins : ';
    }

    if (secs < 10) {
      finalTime += '0$secs';
    } else {
      finalTime += '$secs';
    }

    setState(() {
      timeInSecondsFinal = finalTime;
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      var updateQuery = [
        "UPDATE result SET  game_tot_time=? WHERE match_id=?",
        [timeInSecondsMain, currentMatchID],
        {"calledMethod": "setTimeData"}
      ];
      controller.execFunction(
          ControllerFunc.db_sqlite, ControllerSubFunc.db_update, updateQuery);
    } else {}
  }

  //-----------------------

  // HIDE OR SHOW BACK ARROW OF THE HEADER-----------------------------------------
  showHideHeaderBackArrow(bool trueFalse) {
    setState(() {
      _showBackBtn = trueFalse;
    });
  }

  // HIDE OR SHOW BACK ARROW OF THE HEADER-----------------------------------------
  showHideHeaderShareArrow(bool trueFalse) {
    setState(() {
      _showShareBtn = trueFalse;
    });
  }

  // HIDE OR SHOW BOTTOM BAR-------------------------------------------------------
  showHideBottomBar(bool trueFalse) {
    setState(() {
      _showBottom = trueFalse;
    });
  }

  // CHANGE THE TAB ON THE FLY WITHOUT BOTTOM BAR CHANGE----------------------------
  funcOnSelectedChangeTab(RoutingData setPageVal, bool bottomBarShowHide, bool backArrowShowHide) {
      
      setState(() {
        if (_selectedPage != 14 && _selectedPage != 17) {
          _flyPage = _selectedPage;     
        }
      });
      setPageIndex(setPageVal.index);
      showHideBottomBar(bottomBarShowHide);
      showHideHeaderBackArrow(backArrowShowHide);

  }

  // CHANGE THE TAB ON THE FLY WITH BOTTOM BAR CHANGE--------------------------------
  onPageChanged(
      RoutingData setPageVal, bool bottomBarShowHide, bool backArrowShowHide) {
    forceBottomNavChange(setPageVal.index);
  }

  // SCORE DATA SETUPS-----------------------------------------------------------------
  int gameTimeInSeconds = 0;
  static int pauseGameTiem;
  funcSetGameTimeInSeconds(int x) {
    setState(() {
      gameTimeInSeconds = x;
      pauseGameTiem = x;
    });
  }

  bool gameNotxStarted = true;
  funcSetGameStart(bool x) {
    setState(() {
      if (x == true) {
        gameNotxStarted = true;
      } else {
        gameNotxStarted = false;
      }
    });
  }

  bool gameFinish = false;
  funcSetGameFinish(bool x) {
    setState(() {
      if (x == true) {
        gameFinish = true;
      } else {
        gameFinish = false;
      }
    });
  }

  int playerServe = 1;
  funcSetplayerServe(int x) {
    setState(() {
      playerServe = x;
    });
  }

  bool player1Defalut = false;
  bool player2Defalut = false;

  funcSetplayerDefalut(bool plyr1, bool plyr2) {
    setState(() {
      if (plyr1) {
        player1Defalut = true;
      } else {
        player1Defalut = false;
      }

      if (plyr2) {
        player2Defalut = true;
      } else {
        player2Defalut = false;
      }
    });
  }

  int rallyCount = 1;
  funcKeepRallyCount(int x) {
    setState(() {
      rallyCount = x;
    });
  }

  var tempPlayerScores = {};
  funcSetTempPlayerScores(arg) {
    setState(() {
      tempPlayerScores = arg;
    });
  }

  var scoreArguments = {};
  funcOnScoreArgumentSaving(arg) {
    setState(() {
      scoreArguments = arg;
    });
  }

  funcGameDataUpdate(arg) {
    funcOnScoreArgumentSaving(arg);
  }

  //share buton on click
  screenShot() async {
    RenderRepaintBoundary boundary = screen.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    //Share.text('my text title', 'This is my text to share with other applications.', 'text/plain');
    //var filePath = await ImagePickerSaver.saveFile(fileData:byteData.buffer.asUint8List() );
    //print(filePath);

    // print("share image");

    try {
      await Share.file('TenniZo Share score', 'tenizo.png', byteData.buffer.asUint8List(), 'image/png',text: 'TenniZo Score data');      
    } catch (e) {
      // print('error: $e');
    }


    return "sdf";

  }

  // MAIN WIDGET-----------------------------------------
  @override
  Widget build(BuildContext context) {
    final _pageOptions = [
      Players(
          onSelected: this.funcOnSelectedChangeTab,
          onArgumentSaving: this.onArgumentSaving,
          userName:UserDetails.username),
      StadiumList(
          onSelected: this.funcOnSelectedChangeTab,
          onArgumentSaving: this.onArgumentSaving,
          userName: UserDetails.username,),
      Score(
          gameTimeInSeconds: this.gameTimeInSeconds,
          gameFinish: this.gameFinish,
          playerServe: this.playerServe,
          player1Defalut: this.player1Defalut,
          player2Defalut: this.player2Defalut,
          rallyCount: this.rallyCount,
          tempPlayerScores: this.tempPlayerScores,
          scoreArguments: this.scoreArguments,
          funcSetGameTimeInSeconds: this.funcSetGameTimeInSeconds,
          funcSetGameStart: this.funcSetGameStart,
          funcSetGameFinish: this.funcSetGameFinish,
          funcSetplayerServe: this.funcSetplayerServe,
          funcKeepRallyCount: this.funcKeepRallyCount,
          funcSetplayerDefalut: this.funcSetplayerDefalut,
          setTempPlayerScores: this.funcSetTempPlayerScores,
          //Page Navigations-------------------------
          toggleStartState: this.toggleStartState,
          onSelected: this.funcOnSelectedChangeTab,
          onArgumentSaving: this.onArgumentSaving,
          onPageChanged: this.onPageChanged,
          toggleNavBar: this.toggleNavBar,
          togglePlayStateMain: this.togglePlayStateMain,
          timerStatusMain: this.timerStatusMain,
          timeInSecondsMain: this.timeInSecondsMain),
      Home(
        onSelected: this.funcOnSelectedChangeTab,
        onArgumentSaving: this.onArgumentSaving,
        toggleStartState: this.toggleStartState,
        onScoreArgumentSaving: this.funcOnScoreArgumentSaving,
        setTempPlayerScores: this.funcSetTempPlayerScores,
        onPageChanged: this.onPageChanged,
        initAd: initAd,
        showAd: showAd,
        userName: UserDetails.username,
        funcSetGameTimeInSeconds: this.funcSetGameTimeInSeconds,
        funcSetplayerServe: this.funcSetplayerServe,
      ),
      //---------------------------------------------------------- Main List Finished----------------------------------
      GameSettings(
          funcSetGameStart: this.funcSetGameStart,
          toggleStartState: this.toggleStartState,
          onSelected: this.funcOnSelectedChangeTab,
          onScoreArgumentSaving: this.funcOnScoreArgumentSaving,
          userName: UserDetails.username,
          setTempPlayerScores: this.funcSetTempPlayerScores,),
      StadiumRegistration(onSelected: this.funcOnSelectedChangeTab,userName: UserDetails.username,),
      PlayerRegistration(onSelected: this.funcOnSelectedChangeTab,userName: UserDetails.username,),
      PlayerView(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      StadiumView(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      StadiumEdit(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      EditPlayer(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      CourtRegistration(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      CourtView(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      CourtEdit(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      Stats(
        arguments: this.arguments,
        onSelected: this.funcOnSelectedChangeTab,
        onArgumentSaving: this.onArgumentSaving,
        screenShot: this.screenShot,
        screen: this.screen,
        initAd: this.initAd,
        showAd: this.showAd,
      ),
      GameSettingUpdate(
          gameNotxStarted: this.gameNotxStarted,
          scoreArguments: this.scoreArguments,
          toggleStartState: this.toggleStartState,
          onSelected: this.funcOnSelectedChangeTab,
          gameDataUpdate: this.funcGameDataUpdate,
          userName: UserDetails.username,),
      EditUser(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
      FlowChart(
        arguments: this.arguments,
        onSelected: this.funcOnSelectedChangeTab,
        onArgumentSaving: this.onArgumentSaving,
        initAd: this.initAd,
        showAd: this.showAd,
      ),
      HelpPage(
          arguments: this.arguments, onSelected: this.funcOnSelectedChangeTab),
    ];

    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: AppStyle.color_Bg,
        appBar: _showTopBar
            ? TopBar(
                ctx : context,
                headerTitle: customHeaderTitle,
                showBackBtn: _showBackBtn,
                showShareBtn: _showShareBtn,
                onBackClicked: onBackClicked,
                logoutButton: logoutButton,
                onSelected: this.funcOnSelectedChangeTab,
                onPageChanged: this.onPageChanged,
                screenShot: this.screenShot,
                initAd: initAd,
                showAd: showAd,
              )
            : null,
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: _showBottom
            ? BottomBar(
                selectedPage: _selectedPage,
                setPage: setPageIndex,
                bottomNavigationKey: this.bottomNavigationKey)
            : null,
      ),
    );
  }

  @override
  resultFunction(func, subFunc, response) {
    return null;
  }
}
