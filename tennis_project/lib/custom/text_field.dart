import 'package:tenizo/util/app_const.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class TxtBox extends StatefulWidget {
  const TxtBox({
    Key key,
    this.topic,
    this.child,
    this.borderColor,
    this.errorText,
    this.invalidColor,

    // this.color = const Color(0xFFFFE306),
    // this.child,
  }) : super(key: key);

  final Widget child;
  final String topic;
  final borderColor;
  final String errorText;
  final invalidColor;

  // final Color color;
  // final Widget child;

  @override
  State<StatefulWidget> createState() => _TxtBox();
}

class _TxtBox extends State<TxtBox> {
  // @override
  // void initState() {
  //   super.initState();
  // }

  static const form_width = 280.0;
  static double rowHeight = 100.0;
  static const box_height = 48.0;
  static const default_font_size = AppStyles.normal_font;
  static const default_padding = 0.0;
  static const text_box_height = (box_height - default_font_size) / 2.0;

  @override
  Widget build(BuildContext context) {
    var boderColor;
    var invalidColor;
    var errorTextContainer;

    if (widget.borderColor == null) {
      boderColor = AppColors.form_border;
    } else {
      boderColor = widget.borderColor;
    }
    if (widget.invalidColor == null) {
      invalidColor = AppColors.white;
    } else {
      invalidColor = widget.invalidColor;
    }
    if (widget.errorText == null) {
      setState(() {
        rowHeight = 80.0;
      });
      errorTextContainer = Container(
        width: 300.0,
        height: 5,
      );
    } else {
      setState(() {
        rowHeight = 100.0;
      });
      errorTextContainer = Container(
        width: 300.0,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: new Text(
            widget.errorText,
            style: TextStyle(
              color: Colors.red,
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
      );
    }

    return Align(
      child: Container(
        width: form_width,
        height: rowHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Flexible(
                child: Text(
                  widget.topic,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: default_font_size,
                      color: AppColors.black,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Container(
              color: invalidColor,
              child: Container(
                  width: form_width,
                  height: box_height,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      color: boderColor,
                      style: BorderStyle.solid,
                      width: 1.2,
                    ),
                  ),
                  child: widget.child),
            ),
            errorTextContainer,
          ],
        ),
      ),
    );
  }
}
