import 'package:tenizo/styles/app_style.dart';
import 'package:flutter/material.dart';

class CommonPopupOneButton extends StatelessWidget {
  BuildContext context;
  String optionType;
  String message;
  List<String> param;
  Function deleteItem;
  CommonPopupOneButton(
      this.context, this.optionType, this.message, this.param, this.deleteItem);

  @override
  Widget build(BuildContext context) {
    var textDesplay = "";
    var iconDesplay;
    var messageText = message;
    var fontColor;

    if (optionType == "notification") {
      textDesplay = "Notification";
      iconDesplay = Icons.notifications_active;
      fontColor = Colors.black;
    }
    if (optionType == "error") {
      textDesplay = "Error !";
      iconDesplay = Icons.error_outline;
      fontColor = Colors.red;
    }

    return MediaQuery(
      data: MediaQueryData(),
      child: AlertDialog(
        title: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Icon(
                    iconDesplay,
                    size: 30,
                    color: fontColor,
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: new Text(
                      textDesplay,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                          color: fontColor,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        content: new Text(messageText,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                color: fontColor,
                fontStyle: FontStyle.normal)),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 50.0, bottom: 10.0),
            child: Container(
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SizedBox(
                        width: 170,
                        child: new RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              side: BorderSide(
                                  color: AppStyle.color_Head,
                                  style: BorderStyle.solid)),
                          color: AppStyle.color_Head,
                          child: new Text(
                            "OK",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 20,
                                color: Colors.white,
                                fontStyle: FontStyle.normal),
                          ),
                          onPressed: () =>
                              {Navigator.of(context).pop(), deleteItem(param)},
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
