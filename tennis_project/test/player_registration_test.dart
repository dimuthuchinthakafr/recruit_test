import 'package:tenizo/util/player_registration_util.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("Return the correct age", () {
    var today1 = new DateTime(2019, 05, 21);
    var today2 = new DateTime(2025, 05, 21);

    var birthDay1 = new DateTime(2018, 05, 21);
    var birthDay2 = new DateTime(1950, 05, 21);
    var birthDay3 = new DateTime(1989, 05, 21);
    var birthDay4 = new DateTime(2015, 05, 21);

    expect(PlayerRegistrationPageUtils.ageCalculation(today1, birthDay1), "1");
    expect(PlayerRegistrationPageUtils.ageCalculation(today1, birthDay2), "69");
    expect(PlayerRegistrationPageUtils.ageCalculation(today1, birthDay3), "30");
    expect(PlayerRegistrationPageUtils.ageCalculation(today2, birthDay2), "75");
    expect(PlayerRegistrationPageUtils.ageCalculation(today2, birthDay4), "10");
  });

  test("Return the correct date format", () {
    var date1 = new DateTime(2015, 01, 21);
    var date2 = new DateTime(2015, 02, 21);
    var date3 = new DateTime(2015, 03, 21);
    var date4 = new DateTime(2015, 04, 21);
    var date5 = new DateTime(2015, 05, 21);
    var date6 = new DateTime(2015, 06, 21);
    var date7 = new DateTime(2015, 07, 21);
    var date8 = new DateTime(2015, 08, 21);
    var date9 = new DateTime(2015, 09, 21);
    var date10 = new DateTime(2015, 10, 21);
    var date11 = new DateTime(2015, 11, 21);
    var date12 = new DateTime(2015, 12, 21);

    expect(PlayerRegistrationPageUtils.dateFormatter(date1), "Jan 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date2), "Feb 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date3), "Mar 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date4), "Apr 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date5), "May 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date6), "Jun 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date7), "Jul 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date8), "Aug 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date9), "Sept 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date10), "Oct 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date11), "Nov 21 , 2015");
    expect(PlayerRegistrationPageUtils.dateFormatter(date12), "Dec 21 , 2015");
  });
}
